"""
The following code was adapted from the Bank Reserves model included in Netlogo
Model information can be found at:
http://ccl.northwestern.edu/netlogo/models/BankReserves
Accessed on: November 2, 2017
Author of NetLogo code:
    Wilensky, U. (1998). NetLogo Bank Reserves model.
    http://ccl.northwestern.edu/netlogo/models/BankReserves.
    Center for Connected Learning and Computer-Based Modeling,
    Northwestern University, Evanston, IL.
"""

import mesa
import time
import numpy as np
import random

from .random_walk import RandomWalker

class Platform(mesa.Agent):
    def __init__(self, id, model):
        # initialize the parent class with required parameters
        super().__init__(id, model)
        np.random.seed(int((time.time()-int(time.time()))*10000000)) # set a truly random number according to the current time with milliseconds
        self.assets = np.random.lognormal(mean=3, sigma=1) # totune
        self.innovationCapability = [self.assets/10] # list because quality will use IC history ; first estimation, could be better
        self.marketIntegration = 0
        self.quality = 0
        self.users = []
        self.sellers = []
        self.netIncome = 0 # allows to collect data about it

    def innovate(self): # comprises all platform attributes update except asset
        np.random.seed(int((time.time()-int(time.time()))*10000000))

        # update marketIntegration
        self.marketIntegration = len(self.sellers)*(1.5 - self.model.pcoop)
        # update innovationCapability
            # generate income
        income = 0.01 * self.assets # service income = return on investment of assets
        for s in self.model.sellers:
            if s.unique_id in self.sellers:
                income += 0.01*s.wealth # advertising income, budget of seller allocated to advertising

        costs = 5 + len(self.users)*0.01 + self.innovationCapability[-1] # fixed + variable

        self.netIncome = income - costs

        data = len(self.users) * self.marketIntegration * (1 - self.model.ppriva) / 10000
        predation = self.marketIntegration * len(self.sellers) / 1000
        print("id:", self.unique_id)
        print("income", income)
        print("costs", costs)
        print("data", data)
        print("predation", predation)

        self.innovationCapability.append(((self.netIncome + data + predation) * np.random.normal(1,0.25))/100) # add new IC to the list


        # initialise quality
        self.quality = sum(self.innovationCapability[-10:])/len(self.innovationCapability[-10:]) # get average of last 10 elements
    #
    # def update_sellers(self):
    #         for seller in self.sellers:
    #             seller.calculate_switching_cost(self.quality, self.model.data_portability, len(self.users))
    # -> I did update seller in the seller function decide_switch itself


    def investAndLoose(self):
        self.assets += self.innovationCapability[-1]*5 # multiplying by 5 is just a curve magnifier
        assets_list = [a.assets for a in self.model.platforms]
        assets_max = sorted(assets_list, reverse=True)[0]

        np.random.seed(int((time.time()-int(time.time()))*10000000))
        if self.assets == assets_max and random.random() < 0.50 * self.model.pantiante:
            self.assets -= 0.01*self.assets

    def step(self):
        self.innovate()
        self.investAndLoose()
        #print(self.assets)

        # platform.update_sellers()

        # # users switch if decided
        # for user in self.users:
        #     current_platform = user.platform
        #     potential_platform = np.random.choice(self.platforms)
        #     user.calculate_switching_cost(len(current_platform.users), current_platform.quality)
        #     potential_benefits = potential_platform.quality - current_platform.quality - user.switching_cost
        #     switch_prob = 1 / (1 + np.exp(-potential_benefits))
        #     if np.random.random() < switch_prob:
        #         current_platform.users.remove(user)
        #         user.platform = potential_platform
        #         potential_platform.users.append(user)
        #
        # # sellers switch if decided
        # for seller in self.sellers:
        #     current_platform = seller.platform
        #     potential_platform = np.random.choice(self.platforms)
        #     seller.calculate_switching_cost(len(current_platform.users), potential_platform.quality)
        #     potential_benefits = potential_platform.quality - current_platform.quality - seller.switching_cost
        #     switch_prob = 1 / (1 + np.exp(-potential_benefits))
        #     if np.random.random() < switch_prob:
        #         current_platform.sellers.remove(seller)
        #         seller.platform = potential_platform
        #         potential_platform.sellers.append(seller)

#
# class User(mesa.Agent):
#     def __init__(self, unique_id, model):
#         # initialize the parent class with required parameters
#         super().__init__(unique_id, model)
#         self.platform = None
#         self.switchingCost = 0
#
#
# import mesa
# import time
# import numpy as np
# from .random_walk import RandomWalker
#
# class Platform(mesa.Agent):
#     def __init__(self, unique_id, model):
#         # initialize the parent class with required parameters
#         super().__init__(unique_id, model)
#         np.random.seed(int(time.time()*10000000)) # set a truly random number according to the current time with milliseconds
#         self.asset = np.random.lognormal(mean=3, sigma=1) # totune
#         self.innovationCapability = [] # list because quality will use IC history
#         self.marketIntegration = 0
#         self.quality = 0
#         self.users = []
#         self.sellers = []
#
#     def innovate(self, p): # comprises all platform attributes update except asset
#         np.random.seed(int(time.time()*10000000))
#
#         # update marketIntegration
#         self.marketIntegration = len(self.sellers)*(1.5 - p.coop)
#         # update innovationCapability
#             # generate income
#         income = 0
#         for s in self.sellers:
#             income += 1 # todo: do it according to sellers Wealth, or do it with length of sellers list
#             # and todo: implement income with commission and service income
#         data = len(self.users) * self.marketIntegration * (1 - p.priva)
#         predation = numpy.log(self.marketIntegration * len(self.sellers))
#
#         self.innovationCapability.append(np.log(income * data * predation * np.random.normal(1,0.25))) # add new IC to the list
#
#
#         # update quality
#         self.quality = sum(self.innovationCapability[-10:])/len(self.innovationCapability[-10:]) # get average of last 10 elements

# def update_sellers(self):
#         for seller in self.sellers:
#             seller.calculate_switching_cost(self.quality, self.model.data_portability, len(self.users))

# New part (Yet to be modifed )

class User(mesa.Agent):
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.platform = None
        self.switchingCost = 0

    def calculate_switching_cost(self, nb_users, quality):
        # Calculate switching cost using the given formula
        SC = (nb_users/100*quality)*(1 - self.model.pinter) # pinter represents data portability policy
        self.switchingCost = SC
        return SC

    # def calculate_benefit_switch(self, Q_new, Q_current, switching_cost):
    #     # Calculate the benefits of switching
    #     benefit_switch = Q_new - Q_current - switching_cost
    #     return benefit_switch

    def probability_of_switching(self, SC):
        # Calculate the probability of switching using the logistic function
        P_switch = 1 / (1 + np.exp(SC/10)) # divided by 8 otherwise prob probability will always be too low
        if self.unique_id == 1200 or self.unique_id == 10000:
            print("pswitch", P_switch)
            print("quality",self.platform.quality)
        return P_switch

    def decide_switch(self):
        # if self.platform:
        #     Q_current = self.platform.quality
        # else:
        #     Q_current = 1

        # Q_new = new_platform.quality
        nb_users = len(self.platform.users) # if self.platform else 0
        # privacy_concern = self.model.privacy_concern
        # Calculate switching cost
        SC = self.calculate_switching_cost(nb_users, self.platform.quality)

        # Calculate benefit of switching
        # benefit_switch = self.calculate_benefit_switch(Q_new, Q_current, SC)

        # Calculate probability of switching
        P_switch = self.probability_of_switching(SC)

        # Decide to switch based on probability
        if np.random.random() < P_switch:
            # reallocate to a new platform: select a new platform to which I hesitated to switch to:
            platforms = [platform for platform in self.model.platforms if platform.unique_id != self.platform.unique_id]
            platform_qualities = [platform.quality for platform in platforms]
            total_quality = sum(platform_qualities)
            weights = [quality / (1 + total_quality) for quality in platform_qualities]
            new_platform = random.choices(platforms, weights=weights, k=1)[0]
            # delete me from other platform
            self.platform.users.remove(self.unique_id)
            # tie me with a new platform
            self.platform = new_platform
            self.platform.users.append(self.unique_id)
            return True
        return False

    def step(self):
        self.decide_switch()

#
# class User(mesa.Agent):
#     def __init__(self, unique_id, model):
#         super().__init__(unique_id, model)
#         self.platform = None
#         self.switching_cost = 0
#
#     def calculate_switching_cost(self, num_users, platform_quality, data_portability, privacy_concern):
#         self.switching_cost = (
#             np.log(num_users) +
#             (1 / platform_quality) +
#             (1 - data_portability) * privacy_concern
#         )

class Seller(mesa.Agent):
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.platform = None
        self.switchingCost = 0
        np.random.seed(int((time.time()-int(time.time()))*10000000)) # set a truly random number according to the current time with milliseconds
        self.wealth = np.random.lognormal(mean=4, sigma=1) # todo make wealth dynamic according to ads returns, but it would need entire company income model

    def calculate_switching_cost(self, nb_users, quality):
        # Calculate switching cost using the given formula
        SC = (nb_users/1000*quality)*(1 - self.model.pinter) # pinter represents data portability policy
        self.switchingCost = SC
        return SC

    # def calculate_benefit_switch(self, Q_new, Q_current, switching_cost):
    #     # Calculate the benefits of switching
    #     benefit_switch = Q_new - Q_current - switching_cost
    #     return benefit_switch

    def probability_of_switching(self, SC):
        # Calculate the probability of switching using the logistic function
        P_switch = 1 / (1 + np.exp(SC/4)) # divided by 4 otherwise prob probability will always be too low
        return P_switch

    def decide_switch(self):
        # if self.platform:
        #     Q_current = self.platform.quality
        # else:
        #     Q_current = 1

        nb_sellers = len(self.platform.sellers) if self.platform else 0

        # Calculate switching cost
        SC = self.calculate_switching_cost(nb_sellers, self.platform.quality)

        # Calculate probability of switching
        P_switch = self.probability_of_switching(SC)

        # Decide to switch based on probability
        if np.random.random() < P_switch:
            # reallocate to a new platform: select a new platform to which I hesitated to switch to:
            platforms = [platform for platform in self.model.platforms if platform.unique_id != self.platform.unique_id]
            platform_qualities = [platform.quality for platform in platforms]
            total_quality = sum(platform_qualities)
            seller_wealths = [seller.wealth for seller in self.model.sellers] # optimise by taking out this loop outside each individual agents
            total_seller_wealths = sum(seller_wealths)
            weights = [np.abs(quality / (1 + total_quality) - self.wealth / total_seller_wealths) for quality in platform_qualities] # i.e. the difference in the share of wealth/asset
                    # of each platform/seller couple will defines the weights for random attribution so the richest seller will have higher chance to choose a rich (qualitative) platform
            new_platform = random.choices(platforms, weights=weights, k=1)[0]
            # delete me from other platform
            self.platform.sellers.remove(self.unique_id)
            # tie me with a new platform
            self.platform = new_platform # todo define new_platform
            self.platform.sellers.append(self.unique_id)
            return True
        return False

    def step(self):
        self.decide_switch()










#
#
# class Bank(mesa.Agent):
#     def __init__(self, unique_id, model, reserve_percent=50):
#         # initialize the parent class with required parameters
#         super().__init__(unique_id, model)
#         # for tracking total value of loans outstanding
#         self.bank_loans = 0
#         """percent of deposits the bank must keep in reserves - this is set via
#            Slider in server.py"""
#         self.reserve_percent = reserve_percent
#         # for tracking total value of deposits
#         self.deposits = 0
#         # total amount of deposits in reserve
#         self.reserves = (self.reserve_percent / 100) * self.deposits
#         # amount the bank is currently able to loan
#         self.bank_to_loan = 0
#
#     """update the bank's reserves and amount it can loan;
#        this is called every time a person balances their books
#        see below for Person.balance_books()"""
#
#     def bank_balance(self):
#         self.reserves = (self.reserve_percent / 100) * self.deposits
#         self.bank_to_loan = self.deposits - (self.reserves + self.bank_loans)
#
#
# # subclass of RandomWalker, which is subclass to Mesa Agent
# class Person(RandomWalker):
#     def __init__(self, unique_id, pos, model, moore, bank, rich_threshold):
#         # init parent class with required parameters
#         super().__init__(unique_id, pos, model, moore=moore)
#         # the amount each person has in savings
#         self.savings = 0
#         # total loan amount person has outstanding
#         self.loans = 0
#         """start everyone off with a random amount in their wallet from 1 to a
#            user settable rich threshold amount"""
#         self.wallet = self.random.randint(1, rich_threshold + 1)
#         # savings minus loans, see balance_books() below
#         self.wealth = 0
#         # person to trade with, see do_business() below
#         self.customer = 0
#         # person's bank, set at __init__, all people have the same bank in this model
#         self.bank = bank
#
#     def do_business(self):
#         """check if person has any savings, any money in wallet, or if the
#         bank can loan them any money"""
#         if self.savings > 0 or self.wallet > 0 or self.bank.bank_to_loan > 0:
#             # create list of people at my location (includes self)
#             my_cell = self.model.grid.get_cell_list_contents([self.pos])
#             # check if other people are at my location
#             if len(my_cell) > 1:
#                 # set customer to self for while loop condition
#                 customer = self
#                 while customer == self:
#                     """select a random person from the people at my location
#                     to trade with"""
#                     customer = self.random.choice(my_cell)
#                 # 50% chance of trading with customer
#                 if self.random.randint(0, 1) == 0:
#                     # 50% chance of trading $5
#                     if self.random.randint(0, 1) == 0:
#                         # give customer $5 from my wallet
#                         # (may result in negative wallet)
#                         customer.wallet += 5
#                         self.wallet -= 5
#                     # 50% chance of trading $2
#                     else:
#                         # give customer $2 from my wallet
#                         # (may result in negative wallet)
#                         customer.wallet += 2
#                         self.wallet -= 2
#
#     def balance_books(self):
#         # check if wallet is negative from trading with customer
#         if self.wallet < 0:
#             # if negative money in wallet, check if my savings can cover the balance
#             if self.savings >= (self.wallet * -1):
#                 """if my savings can cover the balance, withdraw enough
#                 money from my savings so that my wallet has a 0 balance"""
#                 self.withdraw_from_savings(self.wallet * -1)
#             # if my savings cannot cover the negative balance of my wallet
#             else:
#                 # check if i have any savings
#                 if self.savings > 0:
#                     """if i have savings, withdraw all of it to reduce my
#                     negative balance in my wallet"""
#                     self.withdraw_from_savings(self.savings)
#                 # record how much money the bank can loan out right now
#                 temp_loan = self.bank.bank_to_loan
#                 """check if the bank can loan enough money to cover the
#                    remaining negative balance in my wallet"""
#                 if temp_loan >= (self.wallet * -1):
#                     """if the bank can loan me enough money to cover
#                     the remaining negative balance in my wallet, take out a
#                     loan for the remaining negative balance"""
#                     self.take_out_loan(self.wallet * -1)
#                 else:
#                     """if the bank cannot loan enough money to cover the negative
#                     balance of my wallet, then take out a loan for the
#                     total amount the bank can loan right now"""
#                     self.take_out_loan(temp_loan)
#         else:
#             """if i have money in my wallet from trading with customer, deposit
#             it to my savings in the bank"""
#             self.deposit_to_savings(self.wallet)
#         # check if i have any outstanding loans, and if i have savings
#         if self.loans > 0 and self.savings > 0:
#             # check if my savings can cover my outstanding loans
#             if self.savings >= self.loans:
#                 # payoff my loans with my savings
#                 self.withdraw_from_savings(self.loans)
#                 self.repay_a_loan(self.loans)
#             # if my savings won't cover my loans
#             else:
#                 # pay off part of my loans with my savings
#                 self.withdraw_from_savings(self.savings)
#                 self.repay_a_loan(self.wallet)
#         # calculate my wealth
#         self.wealth = self.savings - self.loans
#
#     # part of balance_books()
#     def deposit_to_savings(self, amount):
#         # take money from my wallet and put it in savings
#         self.wallet -= amount
#         self.savings += amount
#         # increase bank deposits
#         self.bank.deposits += amount
#
#     # part of balance_books()
#     def withdraw_from_savings(self, amount):
#         # put money in my wallet from savings
#         self.wallet += amount
#         self.savings -= amount
#         # decrease bank deposits
#         self.bank.deposits -= amount
#
#     # part of balance_books()
#     def repay_a_loan(self, amount):
#         # take money from my wallet to pay off all or part of a loan
#         self.loans -= amount
#         self.wallet -= amount
#         # increase the amount the bank can loan right now
#         self.bank.bank_to_loan += amount
#         # decrease the bank's outstanding loans
#         self.bank.bank_loans -= amount
#
#     # part of balance_books()
#     def take_out_loan(self, amount):
#         """borrow from the bank to put money in my wallet, and increase my
#         outstanding loans"""
#         self.loans += amount
#         self.wallet += amount
#         # decresae the amount the bank can loan right now
#         self.bank.bank_to_loan -= amount
#         # increase the bank's outstanding loans
#         self.bank.bank_loans += amount
#
#     # step is called for each agent in model.BankReservesModel.schedule.step()
#     def step(self):
#         # move to a cell in my Moore neighborhood
#         self.random_move()
#         # trade
#         self.do_business()
#         # deposit money or take out a loan
#         self.balance_books()
#         # update the bank's reserves and the amount it can loan right now
#         self.bank.bank_balance()
