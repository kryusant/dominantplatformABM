"""
The following code was adapted from the Bank Reserves model included in Netlogo
Model information can be found at:
http://ccl.northwestern.edu/netlogo/models/BankReserves
Accessed on: November 2, 2017
Author of NetLogo code:
    Wilensky, U. (1998). NetLogo Bank Reserves model.
    http://ccl.northwestern.edu/netlogo/models/BankReserves.
    Center for Connected Learning and Computer-Based Modeling,
    Northwestern University, Evanston, IL.
"""

import mesa
import numpy as np

from .agents import Platform, User, Seller

"""
If you want to perform a parameter sweep, call batch_run.py instead of run.py.
For details see batch_run.py in the same directory as run.py.
"""

# Define a custom scheduler

class CustomScheduler(mesa.time.BaseScheduler):
    def __init__(self, model):
        super().__init__(model)
        self.platform_agents = []
        self.user_agents = []
        self.seller_agents = []

    def add(self, agent):
        super().add(agent)
        if isinstance(agent, Platform):
            self.platform_agents.append(agent)
        elif isinstance(agent, User):
            self.user_agents.append(agent)
        elif isinstance(agent, Seller):
            self.seller_agents.append(agent)

    def step(self):
        # Step all platform agents
        for agent in self.platform_agents:
            agent.step()
        # Step all user agents
        for agent in self.user_agents:
            agent.step()
        # Step all seller agents
        for agent in self.seller_agents:
            agent.step()
        self.steps += 1
        self.time += 1

# Start of datacollector functions

def get_assets_concentration(model):
    assets = [a.assets for a in model.schedule.agents if isinstance(a, Platform)]
    # Calculate the total assets
    total_assets = np.sum(assets)

    # Calculate the market shares
    market_shares = [asset / total_assets * 100 for asset in assets]

    # Calculate the HHI
    hhi = sum([share**2 for share in market_shares])

    return hhi

def get_users_concentration(model):
    nb_users = [len(a.users) for a in model.schedule.agents if isinstance(a, Platform)]
    # Calculate the total assets
    total_nb_users = np.sum(nb_users)

    # Calculate the market shares
    market_shares = [nb_u / total_nb_users * 100 for nb_u in nb_users]

    # Calculate the HHI
    hhi = sum([share**2 for share in market_shares])

    return hhi


def get_market_shares(agent):
    if isinstance(agent, Platform):
        assets = [a.assets for a in agent.model.platforms]
        # Calculate the total assets
        total_assets = np.sum(assets)

        # Calculate the market shares
        market_shares = agent.assets / total_assets * 100

        return market_shares
    else:
        return None

def get_user_shares(agent):
    if isinstance(agent, Platform):
        nb_users = [len(a.users) for a in agent.model.platforms]
        # Calculate the total assets
        total_nb_users = np.sum(nb_users)

        # Calculate the market shares
        market_shares = len(agent.users) / total_nb_users * 100

        return market_shares
    else:
        return None

# def get_platform_assets(model):
#     agent_assets = agent.assets for agent in model.schedule.agents if isinstance(agent, Platform) and agent.unique_id == 1]
#     return agent_assets
#
#
# def get_num_rich_agents(model):
#     """return number of rich agents"""
#
#     rich_agents = [a for a in model.schedule.agents if a.savings > model.rich_threshold]
#     return len(rich_agents)
#
#
# def get_num_poor_agents(model):
#     """return number of poor agents"""
#
#     poor_agents = [a for a in model.schedule.agents if a.loans > 10]
#     return len(poor_agents)
#
#
# def get_num_mid_agents(model):
#     """return number of middle class agents"""
#
#     mid_agents = [
#         a
#         for a in model.schedule.agents
#         if a.loans < 10 and a.savings < model.rich_threshold
#     ]
#     return len(mid_agents)
#
#
# def get_total_savings(model):
#     """sum of all agents' savings"""
#
#     agent_savings = [a.savings for a in model.schedule.agents]
#     # return the sum of agents' savings
#     return np.sum(agent_savings)
#
#
# def get_total_wallets(model):
#     """sum of amounts of all agents' wallets"""
#
#     agent_wallets = [a.wallet for a in model.schedule.agents]
#     # return the sum of all agents' wallets
#     return np.sum(agent_wallets)
#
#

class DominantPlatforms(mesa.Model):
    # # grid height
    # grid_h = 20
    # # grid width
    # grid_w = 20

    """init parameters "init_people", "rich_threshold", and "reserve_percent"
       are all set via Slider"""

    def __init__(
        self,
        # height=grid_h,
        # width=grid_w,
        # init_people=2,
        # rich_threshold=10,
        # reserve_percent=50,
        nb_platforms = 10,
        nb_sellers = 1000,
        nb_users = 10000,
        pantipost = 0.5,
        pantiante = 0,
        pcoop = 0,
        palter = 0,
        pinter = 0,
        ppriva = 0.2,
    ):
        super().__init__()
        self.nb_platforms = nb_platforms
        self.nb_sellers = nb_sellers
        self.nb_users = nb_users
        self.pantipost = pantipost
        self.pantiante = pantiante
        self.pcoop = pcoop
        self.palter = palter
        self.pinter = pinter
        self.ppriva = ppriva

        self.platforms = []
        while self.platforms == [] or get_assets_concentration(self) < 2200:
            self.platforms = []
            self.schedule = mesa.time.BaseScheduler(self) # mesa.time.RandomActivation(self)
        # see datacollector functions above
        # create platforms
            for i in range(self.nb_platforms):
                platform = Platform(i, self)
                self.schedule.add(platform)
                self.platforms.append(platform)


        # create platforms
        self.sellers = []
        for i in range(self.nb_sellers):
            seller = Seller(i + self.nb_platforms, self)
            seller.platform = np.random.choice(self.platforms)  # Assign sellers randomly initially
            seller.platform.sellers.append(seller.unique_id)
            self.schedule.add(seller)
            self.sellers.append(seller)

        # create users
        self.users = []
        for i in range(self.nb_users):
            user = User(i + self.nb_platforms + self.nb_sellers, self)
            # user.platform = np.random.choice(self.platforms)  # Assign users randomly initially
            # user.platform.users.append(user)
            self.schedule.add(user)
            self.users.append(user)

        # assign users proportionally according to platform assets
        uid_counter = 0 # first user id
        total_platform_assets = np.sum([platform.assets for platform in self.platforms])
        for platform in self.platforms:
            nb_users_allocated = int((platform.assets / total_platform_assets * self.nb_users))
            for _ in range(nb_users_allocated):
                self.users[uid_counter].platform = platform
                platform.users.append(self.users[uid_counter].unique_id)
                uid_counter += 1
        while uid_counter < self.nb_users:
            platform = self.platforms[self.nb_platforms - 1]
            self.users[uid_counter].platform = platform
            platform.users.append(self.users[uid_counter].unique_id)
            uid_counter += 1

        self.datacollector = mesa.DataCollector(
            model_reporters={
                'AssetsConcentration': get_assets_concentration,
                'UsersConcentration': get_users_concentration
            },
            agent_reporters={
                "Assets": lambda agent: getattr(agent, "assets", None),
                "UserMktShare": get_user_shares,
                "AssetsMktShares": get_market_shares,
                "marketIntegration": "marketIntegration",
                "innovCapability": lambda a: a.innovationCapability[-1] if isinstance(a, Platform) else None,
                "netIncome": "netIncome"
            },
        )

    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()

        collected_data = self.datacollector.get_agent_vars_dataframe()

        last_step = collected_data.index.get_level_values('Step').max()
        ending_values = collected_data.loc[last_step].head(10)
        print("First step:")
        #print(collected_data.head(10))
        print("Last step:", last_step)
        print(ending_values)







    #
    #     # create a single bank for the model
    #     self.bank = Bank(1, self, self.reserve_percent)
    #
    #     # create people for the model according to number of people set by user
    #     for i in range(self.init_people):
    #         # set x, y coords randomly within the grid
    #         x = self.random.randrange(self.width)
    #         y = self.random.randrange(self.height)
    #         p = Person(i, (x, y), self, True, self.bank, self.rich_threshold)
    #         # place the Person object on the grid at coordinates (x, y)
    #         self.grid.place_agent(p, (x, y))
    #         # add the Person object to the model schedule
    #         self.schedule.add(p)
    #
    #     self.running = True
    #     self.datacollector.collect(self)
    #
    # def step(self):
    #     # tell all the agents in the model to run their step function
    #     self.schedule.step()
    #
    #     # collect data
    #     self.datacollector.collect(self)

    def run_model(self):
        for i in range(self.run_time):
            self.step()
