import mesa
from dominantplatforms.agents import Platform, User, Seller
from dominantplatforms.model import DominantPlatforms

"""
Citation:
The following code was adapted from server.py at
https://github.com/projectmesa/mesa/blob/main/examples/wolf_sheep/wolf_sheep/server.py
Accessed on: November 2, 2017
Author of original code: Taylor Mutch
"""

# The colors here are taken from Matplotlib's tab10 palette
# Green
RICH_COLOR = "#2ca02c"
# Red
POOR_COLOR = "#d62728"
# Blue
MID_COLOR = "#1f77b4"

#
# def person_portrayal(agent):
#     if agent is None:
#         return
#
#     portrayal = {}
#
#     # update portrayal characteristics for each Person object
#     if isinstance(agent, Person):
#         portrayal["Shape"] = "circle"
#         portrayal["r"] = 0.5
#         portrayal["Layer"] = 0
#         portrayal["Filled"] = "true"
#
#         color = MID_COLOR
#
#         # set agent color based on savings and loans
#         if agent.savings > agent.model.rich_threshold:
#             color = RICH_COLOR
#         if agent.savings < 10 and agent.loans < 10:
#             color = MID_COLOR
#         if agent.loans > 10:
#             color = POOR_COLOR
#
#         portrayal["Color"] = color
#
#     return portrayal


# dictionary of user settable parameters - these map to the model __init__ parameters
model_params = {
    "pantipost": mesa.visualization.Slider(
        "Antitrust ex-post policy intensity", 0.5, 0, 1, 0.01, description="ranging from 0 for absent policies, to 1 for intensive policy enforcement and success"
    ),
    "pantiante": mesa.visualization.Slider(
        "Antitrust ex-ante policy intensity", 0, 0, 1, 0.01, description="ranging from 0 for absent policies, to 1 for intensive policy enforcement and success"
    ),
    "pcoop": mesa.visualization.Slider(
        "Cooperative policy intensity", 0, 0, 1, 0.01, description="ranging from 0 for absent policies, to 1 for intensive policy enforcement and success"
    ),
    "palter": mesa.visualization.Slider(
        "Alternative platforms policy intensity", 0, 0, 1, 0.01, description="ranging from 0 for absent policies, to 1 for intensive policy enforcement and success"
    ),
    "pinter": mesa.visualization.Slider(
        "Inter-operativity policy intensity", 0, 0, 1, 0.01, description="ranging from 0 for absent policies, to 1 for intensive policy enforcement and success"
    ),
    "ppriva": mesa.visualization.Slider(
        "Privacy policy intensity", 0.2, 0, 1, 0.01, description="ranging from 0 for absent policies, to 1 for intensive policy enforcement and success"
    )
}

# set the portrayal function and size of the canvas for visualization
# canvas_element = mesa.visualization.CanvasGrid(person_portrayal, 20, 20, 500, 500)

# map data to chart in the ChartModule
# line_chart = mesa.visualization.ChartModule(
#     [
#         {"Label": "Rich", "Color": RICH_COLOR},
#         {"Label": "Poor", "Color": POOR_COLOR},
#         {"Label": "Middle Class", "Color": MID_COLOR},
#     ]
# )
# line_chart = mesa.visualization.ChartModule(
#     [
#         {"Label": "Platform Assets", "Color": "Black"},
#     ],
#     data_collector_name='datacollector'
# )

#
# model_bar = mesa.visualization.BarChartModule(
#     [
#         {"Label": "Rich", "Color": RICH_COLOR},
#         {"Label": "Poor", "Color": POOR_COLOR},
#         {"Label": "Middle Class", "Color": MID_COLOR},
#     ]
# )
#
# agent_bar = mesa.visualization.BarChartModule(
#     [{"Label": "Wealth", "Color": MID_COLOR}],
#     scope="agent",
#     sorting="ascending",
#     sort_by="Wealth",
# )
#
# pie_chart = mesa.visualization.PieChartModule(
#     [
#         {"Label": "Rich", "Color": RICH_COLOR},
#         {"Label": "Middle Class", "Color": MID_COLOR},
#         {"Label": "Poor", "Color": POOR_COLOR},
#     ]
# )

assets_concentration_chart = mesa.visualization.ChartModule(
    [{"Label": "AssetsConcentration", "Color": "#d62728"}],
)

users_concentration_chart = mesa.visualization.ChartModule(
    [{"Label": "UsersConcentration", "Color": "Blue"}],
)

assets_chart = mesa.visualization.BarChartModule(
    [{"Label": "Assets", "Color": "#d62728"}],
    scope="agent",
    sorting="ascending",
    sort_by="Assets",
)

# create instance of Mesa ModularServer
server = mesa.visualization.ModularServer(
    DominantPlatforms,
    [assets_concentration_chart, users_concentration_chart], #model_bar, agent_bar, pie_chart],
    "Dominant platforms sectoral market simulation",
    model_params=model_params,
)
