import mesa
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from dominantplatforms.agents import Platform, User, Seller
from dominantplatforms.model import DominantPlatforms

def run_model(nb_cycles, nb_steps):
    assets_concentration_data = []
    users_concentration_data = []

    for cycle in range(nb_cycles):
        model = DominantPlatforms(ppriva=1)
        for step in range(nb_steps):
            model.step()

        concentration_df = model.datacollector.get_model_vars_dataframe()
        assets_concentration_data.append(concentration_df['AssetsConcentration'].values)
        users_concentration_data.append(concentration_df['UsersConcentration'].values)

    return pd.DataFrame({
        'Step': np.tile(np.arange(nb_steps), nb_cycles),
        'Platforms assets HHI': np.concatenate(assets_concentration_data),
        'Platforms users HHI': np.concatenate(users_concentration_data),
        'Cycle': np.repeat(np.arange(nb_cycles), nb_steps)
    })

nb_cycles = 10
nb_steps = 50

concentration_data = run_model(nb_cycles, nb_steps)

# Create a DataFrame for Seaborn
steps = np.arange(nb_steps)
# df = pd.DataFrame({
#     'Steps': np.tile(steps, nb_cycles),
#     'Concentration': concentration_data.flatten()
# })

# Plot with Seaborn
plt.figure(figsize=(10, 6))
sns.set_style("whitegrid")
sns.lineplot(x='Step', y='value', hue='variable',
             data=pd.melt(concentration_data, ['Step', 'Cycle']), ci='sd')
plt.xlabel('Period')
plt.ylabel('HHI')
plt.title('Average Herfindahl-Hirschman concentration index over periods (with standard deviation)')
plt.legend(loc='upper left')
plt.show()
